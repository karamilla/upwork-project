const express = require("express");
const app = express();

// THE ROUTES
// ==============
const advertisingScripts = require("./routes/advertisingScripts");
const bulletScripts = require("./routes/bulletScripts");
const caseStudyScript = require("./routes/caseStudiesTestimonialsScripts");
const ContentCreationScripts = require("./routes/contentCreationScripts");
const dotcomSecretsScripts = require("./routes/dotcomSecretsScripts");
const EmailScripts = require("./routes/emailScripts");
const ExpertScripts = require("./routes/expertSecrets");
const megaScripts = require("./routes/megaScripts");
const oneFunnelAwayScripts = require("./routes/oneFunnelAwayScripts");
const salesCopyVideoScripts = require("./routes/salesCopyVideoScripts");
const SalesLettersScripts = require("./routes/salesLetterScripts");
const tittleHeadLines = require("./routes/titleHeadlinesSubjectLines");
// ======================================================================================================
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());

app.set("views", "templates");
app.set("view engine", "ejs");

// tester
app.get("/", (req, res) => {
  res.send("hello");
});

// =======================================================
// Setting Routers
app.use("/", advertisingScripts);
app.use("/", bulletScripts);
app.use("/", caseStudyScript);
app.use("/", ContentCreationScripts);
app.use("/", dotcomSecretsScripts);
app.use("/", EmailScripts);
app.use("/", ExpertScripts);
app.use("/", megaScripts);
app.use("/", oneFunnelAwayScripts);
app.use("/", salesCopyVideoScripts);
app.use("/", SalesLettersScripts);
app.use("/", tittleHeadLines);

const port = process.env.PORT || 3000;
app.listen(port);
