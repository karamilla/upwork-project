const express = require("express");
const router = express.Router();
const parent = "sales-letter-scripts";
let template1 = "long-form-sales-letter-script";
let template2 = "ppt-video-sales-letter-scripts";
let template3 = "short-sales-letter-script";

const requestHandler = require("../utils/requestHandler"); // if you use this router, please rename it

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template1}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template1, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template1}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template1, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template2}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template2, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template2}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template2, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template3}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template3, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template3}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template3, action, data, option)
  );
});

module.exports = router;
