const express = require("express");
const router = express.Router();
const parent = "case-studies-testimonials-scripts";
let template1 = "case-study-script";
let template2 = "million-dollar-testimonial-script";

const requestHandler = require("../utils/requestHandler"); // if you use this router, please rename it

/**
 * case-studies-testimonials-scripts /case-study-script fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template1}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template1, action));
});

/**
 * case-studies-testimonials-scripts /case-study-script renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template1}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template1, action, data, option)
  );
});

/**
 * case-studies-testimonials-scripts /million-dollar-testimonial-script fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template2}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template2, action));
});

/**
 * case-studies-testimonials-scripts /million-dollar-testimonial-script renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template2}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;

  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template2, action, data, option)
  );
});

module.exports = router;
