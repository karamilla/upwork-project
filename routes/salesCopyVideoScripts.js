const express = require("express");
const router = express.Router();
const parent = "sales-copy-video-scripts";
let template1 = "amazon-ecommerce-script";
let template2 = "call-to-action-script";
let template3 = "lead-capture-script";
let template4 = "live-event-invitation-script";
let template5 = "magic-bullet-script";
let template6 = "order-bump-script";
let template7 = "origin-story-script";
let template8 = "oto-need-help-done-for-you-script";
let template9 = "oto-next-thing-script";
let template10 = "ppt-opt-in-video-script";
let template11 = "promote-your-webinar-script";
let template12 = "sales-opener-questions-scripts";
let template13 = "special-offer-scripts";
let template14 = "webinar-opt-in-script";
let template15 = "who-what-why-how-script";

const requestHandler = require("../utils/requestHandler"); // if you use this router, please rename it

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template1}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template1, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template1}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template1, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template2}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template2, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template2}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template2, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template3}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template3, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template3}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template3, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template4}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template4, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template4}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template4, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template5}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template5, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template5}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template5, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template6}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template6, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template6}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template6, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template7}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template7, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template7}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template7, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template8}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template8, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template8}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template8, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template9}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template9, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template9}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template9, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template10}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template10, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template10}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template10, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template11}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template11, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template11}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template11, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template12}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template12, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template12}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template12, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template13}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template13, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template13}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template13, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template14}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template14, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template14}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template14, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template15}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template15, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template15}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template15, action, data, option)
  );
});

module.exports = router;
