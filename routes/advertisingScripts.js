const express = require("express");
const router = express.Router();
const parent = "advertising-scripts";
let template1 = "curiosity-ad-copy-scripts";
let template2 = "facebook-newsfeed-ad-scripts";
let template3 = "ppc-ad-scripts";
let template4 = "stealth-close-scripts";

const requestHandler = require("../utils/requestHandler"); // if you use this router, please rename it

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template1}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template1, action));
});

// USE THIS AS AN EXAMPLE FOR THE OTHERS
/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template1}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  // NOTE: data hsould be req.body the template itself will look at data.field1
  // EXAMPLE: object to send in postman
  /*
    {
      "data": {
        "field1": "test1",
        "field2": "test2",
        "field3": "test3",
        "field4": "test4",
        "field5": "test5",
        "field6": "test6",
        "field7": "test7",
        "field8": "test8",
        "field9": "test9",
        "field10": "test10",
        "field11": "test11",
        "field12": "test12",
        "field13": "test13",
        "field14": "test14",
        "field15": "test15",
        "field16": "test16"
      }
    }
  */
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template1, action, data, option)
  );
});

// /END EXAMPLE
/**
 * Advertising-scripts/facebook-newsfeed-ad-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template2}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template2, action));
});

/**
 * Advertising-scripts/facebook-newsfeed-ad-scripts html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template2}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;

  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template2, action, data, option)
  );
});

/**
 * Advertising-scripts/ppc-ad-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template3}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template3, action));
});

/**
 * Advertising-scripts/ppc-ad-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template3}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;

  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template3, action, data, option)
  );
});

/**
 * Advertising-scripts/stealth-close-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template4}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template4, action));
});

/**
 * Advertising-scripts/stealth-close-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template4}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;

  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template4, action, data, option)
  );
});

module.exports = router;
