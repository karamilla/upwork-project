const express = require("express");
const router = express.Router();
const parent = "expert-secrets";
let template1 = "5-curiosity-hooks-script";
let template2 = "5-minute-perfect-webinar-wizard";
let template3 = "ask-campaign-script";
let template4 = "epiphany-bridge-script";
let template5 = "opportunity-switch-headline-title-script";
let template6 = "short-epiphany-bridge-script";
let template7 = "the-big-domino";
let template8 = "who-what-statement-script";

const requestHandler = require("../utils/requestHandler"); // if you use this router, please rename it

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template1}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template1, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template1}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template1, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template2}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template2, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template2}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template2, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template3}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template3, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template3}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template3, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template4}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template4, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template4}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template4, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template5}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template5, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template5}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template5, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template6}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template6, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template6}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template6, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template7}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template7, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template7}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template7, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template8}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template8, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template8}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template8, action, data, option)
  );
});

module.exports = router;
