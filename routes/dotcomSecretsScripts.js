const express = require("express");
const router = express.Router();
const parent = "dotcom-secrets-scripts";
let template1 = "amazon-ecommerce-script";
let template2 = "call-to-action-script";
let template3 = "lead-capture-script";
let template4 = "live-event-invitation-script";
let template5 = "magic-bullet-script";
let template6 = "order-bump-script";
let template7 = "origin-story-script";
let template8 = "oto-need-help-done-for-you-script";
let template9 = "oto-next-thing-script";
let template10 = "ppt-opt-in-video-script";
let template11 = "promote-your-webinar-script";
let template12 = "sales-opener-questions-scripts";
let template13 = "special-offer-scripts";
let template14 = "star-story-solution-script";
let template15 = "webinar-opt-in-script";
let template16 = "who-what-why-how-script";

const requestHandler = require("../utils/requestHandler"); // if you use this router, please rename it

/**
 * dotcom-secrets-scripts/amazon-ecommerce-script fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template1}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template1, action));
});

/**
 * dotcom-secrets-scripts/amazon-ecommerce-script renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template1}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template1, action, data, option)
  );
});

// =================================

/**
 * dotcom-secrets-scripts/call-to-action-script fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template2}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template2, action));
});

/**
 * dotcom-secrets-scripts/call-to-action-script renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template2}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template2, action, data, option)
  );
});

// =================================

/**
 * dotcom-secrets-scripts/lead-capture-script fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template3}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template3, action));
});

/**
 * dotcom-secrets-scripts/lead-capture-script renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template3}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template3, action, data, option)
  );
});

// ========================================

/**
 * dotcom-secrets-scripts/live-event-invitation-script fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template4}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template4, action));
});

/**
 * dotcom-secrets-scripts/live-event-invitation-script renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template4}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template3, action, data, option)
  );
});
// ===========================================

/**
 * dotcom-secrets-scripts/magic-bullet-script fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template5}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template5, action));
});

/**
 * dotcom-secrets-scripts/magic-bullet-script renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template5}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template5, action, data, option)
  );
});

// ===========================================

/**
 * dotcom-secrets-scripts/order-bump-script fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template6}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template6, action));
});

/**
 * dotcom-secrets-scripts/order-bump-script renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template6}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template6, action, data, option)
  );
});

// ======================================

/**
 * dotcom-secrets-scripts/origin-story-script fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template7}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template7, action));
});

/**
 * dotcom-secrets-scripts/origin-story-script renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template7}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template7, action, data, option)
  );
});

// ==================================================

/**
 * dotcom-secrets-scripts/oto-need-help-done-for-you-script fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template8}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template8, action));
});

/**
 * dotcom-secrets-scripts/oto-need-help-done-for-you-script renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template8}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template8, action, data, option)
  );
});

// ========================================

/**
 * dotcom-secrets-scripts/oto-next-thing-script fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template9}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template9, action));
});

/**
 * dotcom-secrets-scripts/oto-next-thing-script renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template9}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template9, action, data, option)
  );
});

/**
 * dotcom-secrets-scripts/ppt-opt-in-video fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template10}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template10, action));
});

/**
 * dotcom-secrets-scripts/ppt-opt-in-video renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template10}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template10, action, data, option)
  );
});

// =======================================

/**
 * dotcom-secrets-scripts/promote-your-webinar-script fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template11}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template11, action));
});

/**
 * dotcom-secrets-scripts/promote-your-webinar-script renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template11}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template11, action, data, option)
  );
});
// ================================

/**
 * dotcom-secrets-scripts/sales-opener-questions-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template12}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template12, action));
});

/**
 * dotcom-secrets-scripts/sales-opener-questions-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template12}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template12, action, data, option)
  );
});

//======================================

/**
 * dotcom-secrets-scripts/special-offer-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template13}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template13, action));
});

/**
 * dotcom-secrets-scripts/special-offer-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template13}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template13, action, data, option)
  );
});

// ========================================

/**
 * dotcom-secrets-scripts/star-story-solution-script fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template14}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template14, action));
});

/**
 * dotcom-secrets-scripts/star-story-solution-script renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template14}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template14, action, data, option)
  );
});
// =======================================

/**
 * dotcom-secrets-scripts/webinar-opt-in-script fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template15}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template15, action));
});

/**
 * dotcom-secrets-scripts/webinar-opt-in-script renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template15}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template14, action, data, option)
  );
});

// ========================================

/**
 * dotcom-secrets-scripts/who-what-why-how-script fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template16}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template16, action));
});

/**
 * dotcom-secrets-scripts/who-what-why-how-script renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template16}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template16, action, data, option)
  );
});

module.exports = router;
