const express = require("express");
const router = express.Router();
const parent = "bullet-scripts";
let template1 = "brunson-bullet-scripts";
let template2 = "feature-benefit-meaning-fbm-bullet-script";

const requestHandler = require("../utils/requestHandler"); // if you use this router, please rename it

/**
 * bullet-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request§
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template1}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template1, action));
});

/**
 * bullet-scripts/feature-benefit-meaning-fbm-bullet-script html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template1}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;

  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template1, action, data, option)
  );
});

/**
 * bullet-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template2}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template2, action));
});

/**
 * bullet-scripts/feature-benefit-meaning-fbm-bullet-script html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template2}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;

  const data = req.body;

  res.send(
    await requestHandler.postAction(parent, template2, action, data, option)
  );
});

module.exports = router;
