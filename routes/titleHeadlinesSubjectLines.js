const express = require("express");
const router = express.Router();
const parent = "title-headlines-subject-lines";
let template1 = "dirty-little-secrets-script";
let template2 = "email-subject-line-scripts";
let template3 = "killer-headline-scripts";
let template4 = "million-dollar-content-headline-scripts";
let template5 = "million-dollar-sales-headline-scripts";
let template6 = "profitable-title-scripts";
let template7 = "short-headline-scripts";
let template8 = "timeless-classic-headline-scripts";

const requestHandler = require("../utils/requestHandler"); // if you use this router, please rename it

/**
 * title-headlines-subject-lines/dirty-little-secrets-script fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template1}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template1, action));
});

/**
 * title-headlines-subject-lines/dirty-little-secrets-script renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template1}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template1, action, data, option)
  );
});

/**
 * title-headlines-subject-lines/email-subject-line-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template2}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template2, action));
});

/**
 * title-headlines-subject-lines/email-subject-line-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template2}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template2, action, data, option)
  );
});

/**
 * title-headlines-subject-lines/killer-headline-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template3}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template3, action));
});

/**
 * title-headlines-subject-lines/killer-headline-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template3}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template3, action, data, option)
  );
});

/**
 * title-headlines-subject-lines/million-dollar-content-headline-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template4}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template4, action));
});

/**
 * title-headlines-subject-lines/million-dollar-content-headline-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template4}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template4, action, data, option)
  );
});

/**
 * title-headlines-subject-lines/million-dollar-sales-headline-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template5}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template5, action));
});

/**
 * title-headlines-subject-lines/million-dollar-sales-headline-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template5}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template5, action, data, option)
  );
});

/**
 * title-headlines-subject-lines/profitable-title-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template6}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template6, action));
});

/**
 * title-headlines-subject-lines/profitable-title-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template6}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template6, action, data, option)
  );
});

/**
 * title-headlines-subject-lines/short-headline-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template7}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template7, action));
});

/**
 * title-headlines-subject-lines/short-headline-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template7}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template7, action, data, option)
  );
});

/**
 * title-headlines-subject-lines/timeless-classic-headline-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template8}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template8, action));
});

/**
 * title-headlines-subject-lines/timeless-classic-headline-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template8}`, async (req, res) => {
  const action = req.query.action;
  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template8, action, data, option)
  );
});

module.exports = router;
