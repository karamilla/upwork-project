const express = require("express");
const router = express.Router();
const parent = "one-funnel-away-scripts";
let template1 = "epiphany-bridge-script";
let template2 = "hook-story-offer-script";
let template3 = "killer-headline-scripts";
let template4 = "niche-and-offer-detective-wizard";
let template5 = "offer-stack-script";
let template6 = "opt-in-hook-story-script";
let template7 = "oto-need-help-done-for-you-script";
let template8 = "question-hooks-script";
let template9 = "oto-next-thing-script";

const requestHandler = require("../utils/requestHandler"); // if you use this router, please rename it

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template1}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template1, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template1}`, async (req, res) => {
  const action = req.query.action;

  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template1, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template2}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template2, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template2}`, async (req, res) => {
  const action = req.query.action;

  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template2, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template3}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template3, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template3}`, async (req, res) => {
  const action = req.query.action;

  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template3, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template4}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template4, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template4}`, async (req, res) => {
  const action = req.query.action;

  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template4, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template5}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template5, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template5}`, async (req, res) => {
  const action = req.query.action;

  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template5, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template6}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template6, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template6}`, async (req, res) => {
  const action = req.query.action;

  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template6, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template7}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template7, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template7}`, async (req, res) => {
  const action = req.query.action;

  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template7, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template8}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template8, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template8}`, async (req, res) => {
  const action = req.query.action;

  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template8, action, data, option)
  );
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts fields
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.get(`/${parent}/${template9}`, (req, res) => {
  const action = req.query.action;
  res.json(requestHandler.getAction(parent, template9, action));
});

/**
 * Advertising-scripts/curiosity-ad-copy-scripts renderd html
 * @param {Request} req - http request
 * @param {Response} res - http response
 */
router.post(`/${parent}/${template9}`, async (req, res) => {
  const action = req.query.action;

  const option = req.query.option;
  const data = req.body;
  res.send(
    await requestHandler.postAction(parent, template9, action, data, option)
  );
});

module.exports = router;
