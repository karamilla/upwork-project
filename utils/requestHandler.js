const ejs = require("ejs");

/**
 * Get handler
 * @param parentId string
 * @param templateId string
 * @param action string
 */

function getAction(parentId, templateId, action) {
  const fields = require(`../templates/${parentId}/${templateId}/fields.json`);

  // NOTE I AM USING A SWITCH TO BE ABLE TO EASILY ADD ADDITIONAL ACTIONS IN THE FUTURE
  // GET actions
  switch (action) {
    case "fields":
      return fields;
    default:
      return {
        error: true,
        message: "Invalid action recieved for: " + templateId,
      };
  }
}

exports.getAction = getAction;

/**
 * Post handler
 * @param parentId string
 * @param templateId string
 * @param action string
 * @param data object
 */
async function postAction(parentId, templateId, action, data, option) {
  // Check if not empty
  if (undefined === data || Object.keys(data).length === 0) {
    throw new Error("data can not be empty");
  }

  // POST actions
  switch (action) {
    case "render":
      const path = `./templates/${parentId}/${templateId}/template.ejs`;
      const html = await renderTemplate(path, data);

      return returnOption(data, html, option);
    default:
      return {
        error: true,
        message: "Invalid POST action recieved for: " + templateId,
      };
  }
}

exports.postAction = postAction;

function returnOption(data, html, option) {
  const header = `<!DOCTYPE html>
  <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>`;

  const footer = `
       
    </body>
  </html>`;

  switch (option) {
    case "json":
      return { fields: data, template: html };
    case "html":
      return html;
    case "html-page":
      return header + html + footer;
  }
}

/**
 * EJS Render
 * @param fileLocation path of file
 * @param data object to be fused with template
 */
async function renderTemplate(fileLocation, data) {
  return new Promise((resolve) => {
    ejs.renderFile(fileLocation, data, {}, function (err, str) {
      if (err) {
        throw new Error("Oops: " + err);
      }

      resolve(str);
    });
  });
}
